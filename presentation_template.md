# Le templating

## Introduction

### Qu'est ce que le templating ?

Le templating ou système de templates sont des modèles qui permettent de définir la manière dont sera structuré un document. Il existe une forme de templating côté Client ainsi que côté Serveur.

Récemment , le templating se voit plus utilisé côté Client (navigateur) pour plusieurs raisons qui sont celles-ci :

* Les navigateurs ne cessent de s'améliorer.
* Le gain de performance des moteurs d'exécutions sont assez conséquents.
* Permet de récupérer des données serveur à la demande sans changer de page et sans recharger les éléments communs du site tels que sa structure HTML.

## Quelques infos supplémentaires

Le templating permet notamment d'isoler les données et la structure d'une page.Le code source d'un système de template peut-être propriétaire ou open-source.

### Mustache

Client-side , le templating est en majeur partie utilisé avec Mustache pour des applications mobiles ou web. Mustache est un système de template web avec implémentations pour Javascript , Ruby , Python , PHP … Lua etc.. Il est « logic-less » à cause de son manque de « control flow statements » comme « if .. else »
Cependant , il est possible de boucler sur des listes et fonctions. Il existe d'autres bibliothèques basés sur Mustache comme Handlebars qui permet d'utiliser des conditions "if .. else" par exemple.


## Quelques exemples

## Exemple 1

*The simplest template:*

```

Hello {{name}}

```

*Template with section tag:*

```

{{#x}}

Some text

{{/x}}

```

Here, when x is a Boolean value then the section tag acts like an if conditional, but when x is an array then it acts like a foreach loop.

*Template that is not escaped:*

> {{&body}}

> Here, if body contains HTML, it won't be escaped.

## Exemple 2

```

var response = JSON.parse(req.responseText);
for (var i = 0;i<response[1].length;i++){
table.innerHTML += "<li>" + response[1][i] +"<br /><p id='bac> k'>"+ response[2][i]+"<br /><br /><a href="+response[3][i]+">"+response[3][i]> +>"</a></p></li>";}

```

Dans cette exemple , qui est extrait de notre TP2 , nous avons lutter à remplir les balises comme on le souhaitait. Le système de template peut nous permettre de simplifier celà.

*Prenons la ligne :*

```

table.innerHTML += "<li>" + response[1][i] +"<br /><p id='bac\
k'>"+ response[2][i]+"<br /><br /><a href="+response[3][i]+">"+response[3][i> ]+\
"</a></p></li>";

```

*Il suffirait de créer une variable contenant la première partie de la balise HTML comme ci-dessous :*

### Exemple avec Mustache

```
 var context = {
  title: response[1][i],
    data: {
        content: response[2][i],
	 link: response[3][i]
    };

```

*Par la suite il suffit de remplir notre structure avec les variables précédemments créer comme ceci :*

```

  table.innerHTML += "<li>" + {{title}} +"<br /><p id='back'>"+ {{data.content}}+"
  <br /><br /><a href="+data.link+">+data.link+
  "</a></p></li>";

```

*De la même manière , on pourrait stocker notre balise HTML en complétant l'exemple comme ceci :*

```

 var context = {
     	 li : "<li>",
	 eli : "</li">,
	 br : "<br />",
	 p : "<p>",
	 endp : "</p>",
	 a : "</a>",
	 adata : <a href="data.link">data.link</a>
 };

```

*A la même manière que ci-dessus , on peut remplir de cette manière :*

```

table.innerHTML += {{li}} + {{title}} +{{br}}+{{p}}+ {{data.content}}+{{br}}+{{br}}+{{adata}}+{{endp}}+{{eli}};

```

Ainsi un code qui prenait 3 lignes , n'en prends désormais qu'1 seule et est entièrement modifiable , en modifiant le contenu des variables directement plutôt que chercher une string dans un fichier de code qui peut être extrémement long.

# Source

*https://en.wikipedia.org/wiki/Mustache_%28template_system%29*

*https://github.com/janl/mustache.js*

### Pour plus d'informations technique sur le templating (javascript)

*http://sylvainpv.developpez.com/tutoriels/javascript/guide-templating-client/*